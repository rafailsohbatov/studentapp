package az.myapp.studentapp.service;

import az.myapp.studentapp.StudentRepository;
import az.myapp.studentapp.model.Student;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class StudentServiceImpl implements StudentService{

    private final StudentRepository studentRepository;

    @Override
    public List<Student> getStudentList() throws Exception {
        return studentRepository.findAll();
    }

    @Override
    public Student addStudent(Student student) throws Exception {
        return studentRepository.save(student);
    }

    @Override
    public Student updateStudent(Student student,Integer id) throws Exception {
        Student oldStudent = studentRepository.findStudentById(id);
        oldStudent.setId(student.getId());
        oldStudent.setName(student.getName());
        oldStudent.setSurname(student.getSurname());
        Student newStudent = studentRepository.save(student);
        return newStudent;
    }

    @Override
    public void deleteStudent(Integer id) throws Exception {
        Student student = studentRepository.findStudentById(id);
        studentRepository.delete(student);
    }
}
