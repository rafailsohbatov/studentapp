package az.myapp.studentapp.service;

import az.myapp.studentapp.StudentRepository;
import az.myapp.studentapp.model.Student;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;


public interface StudentService {

    List<Student> getStudentList() throws Exception;

    Student addStudent(Student student) throws Exception;
    Student updateStudent(Student student,Integer id) throws Exception;
    void deleteStudent(Integer id) throws Exception;
}
