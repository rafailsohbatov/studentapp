package az.myapp.studentapp.controller;

import az.myapp.studentapp.model.Student;
import az.myapp.studentapp.service.StudentService;
import az.myapp.studentapp.dto.StudentDto;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/student")
@RequiredArgsConstructor
public class StudentController {

    private final StudentService studentService;

    @GetMapping
    public List<Student> getStudentList() throws Exception {
        return studentService.getStudentList();
    }

    @PostMapping("/addStudent")
    public Student addStudent(@RequestBody Student student) throws Exception {
        return studentService.addStudent(student);
    }

    @PutMapping("/updateStudent/{studentId}")
    public Student updateStudent(@RequestBody Student student,@PathVariable Integer studentId) throws Exception {
        return studentService.updateStudent(student, studentId);
    }

    @DeleteMapping("/deleteStudent/{id}")
    public void deleteStudent(@PathVariable Integer id) throws Exception {
        studentService.deleteStudent(id);
    }
}
