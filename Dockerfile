FROM openjdk:17
COPY build/libs/studentapp-0.0.1-SNAPSHOT.jar /studentapptest/
ENTRYPOINT ["java"]
CMD ["java","-jar","/studentapptest/studentapp-0.0.1-SNAPSHOT.jar"]